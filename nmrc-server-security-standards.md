## Standards for NMRC Servers

This is a list of standards for NMRC servers with direct Internet access.

It should be mentioned that the "vortex" system is the gateway system between the internal network and the Internet. All servers with direct Internet access are outside of vortex.

## Basics

**Multi-factor authentication**. Available for any service. Exceptions are those services only exposed to vortex, and even those should have multi-factor authentication if possible. This includes SSH. No access between Internet systems.

**Time**. Timezone set to UTC -06:00 Central Time Zone. NTP in use to sync clocks.

**Logs**. Ensure logging is enabled and functioning for all services that offer authentication, access, and auditing functions of any kind. Logs must be retained for 14 days. Logs should use similar date and time format.

**Operating system settings**. No phone home or telemetry gathering by vendor, no extra services active, all auto-patch and auto-update settings are turned on.

**Firewall**. Assume bastion host, with firewall enabled and a default of deny to all ports. Exceptions are for experimental settings or testing and limited to vortex. After each new service or application is enabled, re-audit ports. Ports are only opened for public web services, email services, and SSH (with Duo two-factor) for remote access during travel.

**VPN**. Currently no access to the NMRC network via VPN, although this will possibly be configured in the future.

**Web services**. All web servers must have strong CSP, strong use of SSL, TLS 1.2/1.3 only. No support for old outdated browsers. Current standard is letsencrypt for SSL.

**Mail services**. All email servers must have strong use of SSL, TLS 1.2/1.3 only. They should make use of tight security settings to prevent spam, attacks, etc. Current standard is Sendmail with a highly-customized configuration.

## General Networking

Maintain a separate network each of the following:
- Work laptops.
- Testing of new apps, services, configurations etc
- Security cameras.
- Shitty IoT devices.
- Weird dedicated networking.

Separate networks can be a vlan or an isolated Wi-Fi. If a part of the UniFi wireless network (via a dedicated SSID), it should also be on a separate vlan.

