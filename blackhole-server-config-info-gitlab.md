# Public Server Inventory

## Basic Information

- [x] Full name: blackhole.nmrc.org
- [x] IP address: 65.70.17.129
- [x] Operating system: Ubuntu 20.04 LTS
- [x] Primary function: GitLab standalone server
    - [x] Package name: gitlab-ee
- [x] SSH access: Yes. Unable to implement Duo Security due to git restrictions, so limited to access from vortex.
- [x] Two factor access (describe): TOTP (for GitLab)
- [x] No phone home: Disabled both GitLab reporting as well as Canonical reporting (for Ubuntu).

---

## Configuration Information

Details should include reasoning behind choices

- [x] DNS - 65.70.17.133, main NMRC DNS server
- [x] NTP - NTP server is set to time.google.com, it supports leap smearing which is standard for NMRC servers. Using Ubuntu's timesyncd.
- [x] Logging - Currently logging is to local host only. Eventually will change to an internal server.
- [x] Firewall - ufw in a simple configuration: All ports are open from 65.70.17.130 (mainly for testing of GitLab services but also SSH access), port 80 (http) and 443 (https) are open to the public. All other traffic is blocked.
```
65.70.17.129/tcp           ALLOW IN    65.70.17.130/tcp          
80/tcp                     ALLOW IN    Anywhere                  
443/tcp                    ALLOW IN    Anywhere                  
```
- [x] Patching information - automated using the unattended-upgrades package
    - [x] Automation details - set to update, upgrade, and autoremove old packages via settings in `/etc/apt/apt.conf.d/50unattended-upgrades` and `/etc/apt/apt.conf.d/20auto-upgrades`. Email notification if errors, auto reboot if needed at 2am.
- [x] Backup details - Awaiting automated access to internal NAS, currently manual.
- [x] Break glass plan - Minimal. Console access to the server is available so repairs, incident investigations, etc can be performed without network access. Worse case scenario is wipe/reload from scratch plus manual backup restoration.

Include other important configuration information here:
- [x] Changes from https://about.gitlab.com/blog/2020/05/20/gitlab-instance-security-best-practices/ applied.

Under Admin Area > Metrics and profiling > Usage statistics
- [x] Uncheck `Enable usage ping` was unchecked.
- [x] Uncheck `Enable Seat Link` under `Seat link`.

Changes to `/etc/gitlab/gitlab.rb` for security purposes:

Logging updated:
```
#logging['logrotate_dateformat'] = nil
logging['logrotate_dateformat'] = "-%Y-%m-%d"
```

Custom CSP - not pleased with script-src using unsafe-eval and style-src using unsafe-inline but this is required for GitLab to operate. Will continue to look for alternatives/patches/settings to remedy this. NOTE: with gitlab-ee 13.7 all calls use a nonce, was able to remove unsafe-eval and unsafe-inline.
```
# Custom CSP
gitlab_rails['content_security_policy'] = {
 'enabled' => true,
 'report_only' => false,
 'directives' => {
   'default_src' => "'self' blackhole.nmrc.org",
   'base_uri' => "'self'",
   'child_src' => "'none'",
   'connect_src' => "'self'",
   'font_src' => "'self'",
   'form_action' => "'self'",
   'frame_ancestors' => "'none'",
   'frame_src' => "'self'",
   'img_src' => "'self' data:",
   'manifest_src' => "'self'",
   'media_src' => "'self'",
   'script_src' => "'self' blackhole.nmrc.org",
   'style_src' => "'self'",
   'worker_src' => "'self'",
 }
```
Ensure SMTP uses TLS. These settings ensured that, confirmed with an examination of mail server logs and via network sniffing using tcpdump:
```
gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = "nmrc.org"
gitlab_rails['smtp_port'] = 25
gitlab_rails['smtp_domain'] = "nmrc.org"
gitlab_rails['smtp_enable_starttls_auto'] = true
gitlab_rails['smtp_tls'] = false # I believe this is the default, left over from testing
```
There were a number of adjustments to SSL/TLS settings. This included using only TLSv1.2 or TLSv1.3, nothing but strong ciphers in the order listed, adjustments to the session settings, and a few other settings. The dhparams were set by running the command `openssl dhparam -out dhparams.pem 4096`. The session ticket reuse was turned off and a more secure curve choice was made. Overall this will increase CPU usage by quite a lot, but as this is intended as a low-traffic server with very few users, I'll go with security over performance. So far the performance hit has not been noticed.
```
#
# Only strong ciphers are used
#
nginx['ssl_ciphers'] = "ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:DHE-RSA-AES256-GCM-SHA384:DHE-RSA-AES128-GCM-SHA256"
#
# Follow preferred ciphers and the order listed as preference
#
nginx['ssl_prefer_server_ciphers'] = "on"
#
# Only allow TLSv1.2 and TLSv1.3
#
nginx['ssl_protocols'] = "TLSv1.2 TLSv1.3"

##! **Recommended in: https://nginx.org/en/docs/http/ngx_http_ssl_module.html**
nginx['ssl_session_cache'] = "builtin:1000  shared:SSL:10m"

##! **Default according to https://nginx.org/en/docs/http/ngx_http_ssl_module.html**
nginx['ssl_session_timeout'] = "5m"

# Should prevent logjam attack etc
nginx['ssl_dhparam'] = "/etc/gitlab/ssl/dhparams.pem" # changed from nil

# experimental
# Turn off session ticket reuse
nginx['ssl_session_tickets'] = "off"
# Pick our own curve instead of what openssl hands us
nginx['ssl_ecdh_curve'] = "secp384r1"
```
There were adjustments made to the nginx server configuration. An attempt to use the `nginx['custom_nginx_config'] = "include /etc/nginx/nmrc.conf"` did not work as the entries needed to be in the main server and http levels from other nginx config files. Therefore I did the following to include a Permissions-Policy, turn off (some) RSS, and as a proof of concept for myself limit certain user agents.
```
#
# Permissions Policy. These settings allow GitLab to work. May need to be
# modified in the future, the main one was the notifications settings but all
# working now.
#
# Added location reference to "turn off" RSS. For now .atom and .rss (in case
# there is any) should suffice. 410 error so automation should leave it alone
# after (hopefully) one try. And yes that is a hard carriage return before the
# "location" line because I could not get a \n to work.
#
# Additionally I filtered out some potential troublesome user agents as well.
# Again, obviously this is easily bypassed but the idea here is to prevent
# simple automation. 410 is used as it should clue in smart automation to not
# repeat the attempt.
nginx['custom_gitlab_server_config'] = "add_header Permissions-Policy \"geolocation=();midi=();notifications=(self);push=();sync-xhr=();microphone=();camera=();magnetometer=();gyroscope=();speaker=(self);vibrate=();fullscreen=(self);payment=();\";
  location ~ .atom$ { return 410; }
  if ($http_user_agent ~* (netcrawl|npbot|malicious|LWP::Simple|BBBike|wget|jorgee|curl)) { return 410; }"
```
The 410 error page really stood out, so I came up with a 404 and 410 that looked similar for some level of "OPSEC".
```
# Why custom pages? Making 410 errors for RSS traffic looked so different,
# so why not make 410 and 404 match?
nginx['custom_error_pages'] = {
  '404' => {
    'title' => 'Blackhole 404',
    'header' => 'HTTP/2 404',
    'message' => 'Blackhole 404'
  },
  '410' => {
    'title' => 'Blackhole 410',
    'header' => 'HTTP/2 410',
    'message' => 'Blackhole 410'
  }
}
```
Changes to `/var/opt/gitlab/grafana/grafana.ini` for security purposes:

This was mainly to make it run properly, but added the `enforce_domain` to prevent DNS rebinding attacks (moot? this isn't exposed to the Internet, but just in case...).
```
enforce_domain = true
root_url = https://blackhole.nmrc.org/-/grafana
serve_from_sub_path = true
```
More specific security settings:
```# from the [session] section:
cookie_secure = true

# from the [security] section:
# the following added based upon Grafana docs on security
# prevent CSRF
cookie_samesite = strict
# sets X-Frame-Options: deny
allow_embedding = false
# sets HSTS
strict_transport_security = true
strict_transport_security_max_age_seconds = 86400
# enforces MIME types in Content-Type headers
x_content_type_options = true
# prevent XSS
x_xss_protection = true
```

Changes for runners, more for performance than security purposes, but nonetheless could help prevent DOS involving runners:

Update to `/etc/gitlab-runner/config.toml`. Note that this is for my home system, so performance hits are not expected nor anticipated. If so I will adjust things, but I did make this adjustment, mainly to prevent autoscaling:
```
# limit concurrent to 2, with a check interval of 10 seconds
concurrent = 2
check_interval = 10
```
