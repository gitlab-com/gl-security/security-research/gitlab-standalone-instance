# GitLab standalone instance

This is an on-going research project to set up and secure a standalone instance of GitLab in a hostile environment. In addition to securing the instance, the instance will be used for personal projects as well as advanced adjustments/tweaks/implementations involving security-research-related projects.

The current address of this instance is https://blackhole.nmrc.org.

The threat modeling assessment of the device is [here](threat-modeling-assessment.md).

The configuration information related to GitLab specifically is located [here](blackhole-server-config-info-gitlab.md).
