## Threat Modeling Assessment

For information see the [Threat Modeling](https://about.gitlab.com/handbook/security/threat_modeling) page in the handbook.

## Blackhole Networking - Threat Model Eval
<!--
Replace "Name of Project" above
  -->

### Description of Project
<!--
Include a brief description of the project. This can be only one sentence if one sentence covers it. Think of this as the "goal" of the project.
  -->

NMRC servers are under constant attack from the Internet, with the vast majority of the attacks being network-based. Ensure that all potential network threats have been mitigated against on server `blackhole.nmrc.org` aka [Blackhole](https://blackhole.nmrc.org/). Blackhole's primary application that is exposed to the Internet is a standalone version of GitLab.

<!--  
NOTE: Each Stage below includes some starter questions, but feel free to use your own or simply sum things up. Leave the comments if you wish, as the may help you during the process.

The questions are intended to help start using the framework. Add and delete questions as needed.
  -->
## Stage I - Define objectives
<!--
 * Business objectives. State the business objective of the project. This can be the purpose of the code, the change to the infrastructure, the goal of the marketing campaign, whatever the objective is.
 * Security, Compliance, and Legal requirements. These are both guidelines and limits in a broad sense. For example, the licensing for a proposed third party application should be in order, the privacy policy of the new SaaS application being considered meets muster, or the new code does not require lowering security standards.
 * Business impact analysis. This should include impact to mission and business processes, recovery processes, budget impact, and system resource requirements.
 * Operational impact. This should include impact to existing processes already in use by operational personnel. For example, extra logging could be introduced that might impact interpretation of system events, increase the number of steps for future changes, or alter documented procedures for advanced troubleshooting.
  -->
### Describe the overall project and the objectives.

**Business objectives**. All NMRC servers have specific purposes and usually have ports that are exposed to the public via the Internet. In order to prevent potential intrusions into NMRC resources, a threat analysis needs to be performed to help in the evaluation of existing threats, mitigation actions to take, and the prioritization of these mitigation efforts.

*Are there any special requirements that must be meant (Security, Compliance, and/or Legal):*

There are security standards associated with NMRC servers and services. These must be met. There are no Compliance or Legal requirements other that a TERMS OF SERVICE that all users of the system must agree with.

(add link to standard, do not add link to TERMS OF SERVICE) 

*Business impact as a result of this project, if the part of a larger project/objective, note that here:*

General server by server, application by application, documentation of the NMRC network including the publishing of all data. The GitLab application is intended to help facilitate and document this process.

*Operational impact to any additional systems, processes, personnel, etc:*

None at this time.

## Stage II - Define technical scope
<!--
* Boundaries of technical environment. These are the boundaries related to the project itself. This should include the type of data (by classification) that will be touched with this project.
* Infrastructure, application, software dependencies. All of these need to be documented, including the level of impact. For example, if the project requires an upgrade to a subsystem's library and the subsystem has access read-only access to RED data, this needs to be documented as it could impact other users of the subsystem as well as introduce a potential attack vector to RED data.
  
This creates the boundaries for the project.
  -->
*What part of the infrastructure will my project reside in?*

The server will reside on the public Internet with a static IP address in the /29 range assigned to NMRC. This will be in the "public" section of the network.

*What dependencies does my project rely on? (code such as libraries, or otherwise)*

Network-facing applications and services, including:

 - The Linux operating system, specifically Ubuntu 20.04 LTS.
 - GitLab Ultimate (gitlab-ee).
 - OpenSSH (openssh-server).

*What permissions does my project require?*

All services being evaluated for this project require administrative permissions for operating, configuring, and general maintenance.

*What components does my project talk to, for authorization or for data reading/writing?*

N/A, all components are considered (at least logically) self-contained.

*Will this project create new data, and what is the classification of that data?*

There are three classifications of data - public, pre-public, and non-public. This server and its main application will create all three.

## Stage III - Application Decomposition
<!--
 * Identify use cases, define application entry points and trust levels even if they are to remain unchanged as a result of the project.
 * Identify actors, assets, services, roles, and data sources.
 * Document data movement (in motion, at rest) via Data Flow Diagramming. Show trust boundaries, including existing as well as proposed changes.
 * Document all data touched and its classification.

This allows mapping of what is important to what is in scope.
  -->

*Who are the intended consumers of this project? Note the different roles that might be involved in the "input" as well as the "output".*

Creators of content on this server will include but not be limited to NMRC personnel. Access to the system requires an NMRC mail account. These creators will provide the content.

The general public will be the audience for the published data, with the main consumer of data being the infosec community.

*Will new roles need to be developed for this project?*

No.

*If new data is being created, there needs to be documentation of requirements for this new data "at rest" and "in transit".*

N/A as self-contained within the GitLab application and most of the data will be public. Regular HTTPS standards used by other NMRC servers will apply for web access.

*What data will my project read or write?*

N/A

*Where does that data come from?*

Stated above.

*Will the data be altered in some fashion that could impact other processes that read the data, either by changing its location, altering (or adding) security measures, or transforming the data into a different format?*

No.

*Does any new data created by the change need labeling, tagging or some other addition/configuration that if not in place could result in exposure, compromise, or some other risk?*

No.

*Based upon previous incidents and past risks, create a list of areas or the types of risks that are most likely to occur.*

Application issues with GitLab components itself.

## Stage IV - Threat Analysis
<!--
 * Probabilistic attack scenario analysis, where any scenario that could happen is at least listed.
 * Regression analysis on security events, where we example events that touch on some of the same or similar components.
 * Threat intel correlation, by taking data from logs, Hackerone reports, incidents, and other sources that can be used to indicate an attack scenario.

This is where we mainly look to document relevant threats and threat patterns to the data we have gathered up until now.
  -->
*Irregardless of possibility or probability, what are the potential threats created with this project?*

 - Expanded NMRC general attack surface. Yet another server to potentially get compromised.
 - Unauthorized data created and served to the public (porn, warez, etc).
 - Unauthorized data alteration.
 - Denial of service attacks.
 - Launch of attacks against a third party from the compromised system.

*Are there base threats we need to consider?*

Due to NMRC members' existing and past jobs, there are several groups of actors that pose a direct threat. To consider the base threats as listed above, one must consider the threat actors themselves. These include the following:
    - Nation-state sponsored attacks. The main adversaries seen have been China, Russia, Iran, North Korea, and several western allied nations including Israel, France, UK, and of course USA.
    - Traditional blackhat attackers, aka straight up criminals.
    - Grayhat hackers looking for systems to test new flaws.
    - Script kiddies looking to make a name for themselves. 

*Are there other systems or components that if flawed could introduce risk to our project (and vice versa)?*

The main one is GitLab as it is and will be the only public-facing system.

*Are there known threats to us that could pose a potential immediate threat to our project once implemented?*

The GitLab product is designed to work out of the box as painlessly as possible, so numerous areas are more open than usual NMRC standards.

*Is there anything about the implementation or creation process itself that could introduce risk?*

To test some of the components requires opening up access to the Internet during implementation instead of off-line configuration and testing first.

*Go through the example risks, and examine each one for possibilities not considered:
   * System compromised or crashed
   * Service compromised or inoperable
   * Authentication bypassed, breached, or rendered inoperable
   * Data exposed, altered, relocated, or deleted*

Completed above.

## Stage V - Vulnerability and weakness analysis
<!--
 * Examine existing vulnerability reports and issues tracking.
 * Threat to existing vulnerability mapping using threat trees.
 * Design Flaw analysis using use and abuse cases.
 * Scorings (CVSS) and Enumeration (CWE/CVE).
 * Impacted systems, sub-systems, data. Are we adding to or altering something that has a history of exploitation. Is the current state vulnerable, and will this change potentially be influenced by or change that assessment.
  
This is where we examine the threats, ranking them and assessing their likelihood.
  -->
*Develop a detailed list of past incidents that involve the same or similar systems/processes/code base. The list should include mitigations to correct the issues, including the time to completion and complexity of the mitigation. The list should also include whether the mitigation could be implemented quicker now (using the past experience to guide through potential trouble spots).*

**Incident**:
Trusted insider. An NMRC member had access to various internal projects including code and zero day exploits. These were shared with Chinese intelligence agents. The member was caught during an attempt to zip up a copy of all internal documents and download them. This was attempted using two different methods - copying data to a web server to allow for direct download as well as scp. While the incident was contained before the worse happened (and the user's credentails and access revoked), the China connection was uncovered after being approached about it by the FBI who confirmed China was where a few code samples from earlier breaches had ended up.
**Future mitigation:**
Using GitLab software will allow for more granular control over data, so even a trusted insider is more likely to leave a paper trail, and internal documents can be more controlled.

**Incident:**
Drunken trusted insider. An NMRC member while inebriated let a hacker friend use their shell account to exploit an evaluation of privilege attack to gain root access. The Internet access to the system was immediately disconnected, then the server was being patched.
**Future mitigation:**
Not really, it was kind of funny.

**Incident (still ongoing):**
Continued attacks against services as well as phishing attempts from numerous nation state groups. This was in part due to a member working at MITRE for a while and having to deal with APT actors pretty much constantly (this is not uncommon for others who work for government and government contractor employers). All attempts involved network services of some type.
**Future mitigation:**
Better overall documentation of incidents.

**Incident:**
A simple blog package was installed and it was compromised within 20 minutes by a script kiddie before even a test blog post could be made. The script kiddie was in the process of trying to install software to automate attacks against other systems. The intruder was kicked out and the package removed.
**Future mitigation:**
A bit nervous of new applications as a result, especially during implementation, but such things as log and network monitoring during install is common.

**Incident:**
An implementation of a web-based mail front end to the mail server was compromised on a test server. The mail server had not been fully implemented and was not compromised. An attempt to install additional malicious software on the test server failed, however since the server was due for a major upgrade to a newer version of the base OS, the decision was made to wipe and re-install from scratch.
**Future mitigation:**
The issue here was a lack of thorough testing before implementation, hopefully this will be different.

**Incident:**
Denial of service attack against all services on two systems. This was after NMRC assistance for Chinese dissidents who were working on protests against their government. The initial attacks were against web, DNS, and mail services in an attempt to disable them. Later these were upgraded to simple bandwidth attacks.
**Future mitigation:**
Numerous tweaks to the public-facing services had to be performed, these are done proactively now.

*Compare and contrast the past incidents with the new list, and use this as a ranking for how likely similar past incidents are likely to occur.*

The main risks, based upon past incidents and current trends, are as follows:
 - Script kiddie attacks will most certainly continue, scans and probes occur at least once per hour twenty four hours a day.
 - Nation state attacks will continue. Current rate is roughly one incident per six weeks.
 - Insiders are considered a risk, but this will be handled via different methods and not be a part of this evaluation, outside of the fact that the GitLab system will not be opened up to outside users.

*If possible use Attack Tree modeling to develop threat trees with the vulnerabilities.*

Not needed.

*Design flaw analysis of the project, to try and link theoretical flaws to existing project.*

Most theoretical flaws will be the result of coding flaws in GitLab code.

*As the threat list moves forward, consider CVSS/CWE mappings, and compare those mappings to post incidents/past flaws found.*

N/A

## Stage VI - Attack modeling
<!--
 * Attack surface analysis for the impacted components.
 * Attack tree development. This is where something like MITRE ATT&CK can assist.
 * Attack -> Vulnerability -> Exploit analysis using attack trees.
 * Summarize the impact and explain each risk.

We go through the threats and turn them into attacks, by examining the attack surface before and after our proposed changes.
  -->
*Make a list of potential threat actors, using the previous gathered data for assistance, list the perceived attack methods each would use against assets associated with this change, and rank the list of attack methods each threat actor would use by likelihood/preferred/document methods of choice.*

1. Script kiddies - security scanning tools, automated attack scripts.
1. Nation state actors - higher quality attacks including zero day.
1. Blackhat attackers - higher quality attacks, however rarely zero day.
1. Insider attacks - N/A to the network-based project thus not in scope

*Make a list of the existing and changed attack surface "points" on the project's "map".*

The main point of attack will be the GitLab standalone instance itself, specifically via the web interface.

*Align past and theoretical threats to the attack surface.*

See above.

*Each risk should be run through an attack tree.*

Not needed.

*Create an impact assessment and explain each risk in terms of possible vs impossible.*

Script kiddies will be using tools and performing large sweeps with tools against any public IP address, looking for low hanging fruit, poor configurations, googledorks, and unpatched services. There could be a potential for ransomware installation if successful, otherwise the damage will most likely be reputational, and decidedly quite embarrassing. Pivoting (attacking a separate system using our compromised system as a launch point) is also a concern, as once compromised the attacker could launch further attacks that cause serious damage to other systems, and potentially involve law enforcement.

Nation state actors tend to use more sophisticated network attacks, and will on occasional use "zero day" attacks. However while the network attacks will continue to happen, there are still way more spear phishing attempts. The main danger here will be an attempt to pivot to other local systems, and even third party/outside organizations, plus the potential for law enforcement at the federal level.

Blackhat attackers usually do not use zero day attacks against security researchers and the home systems they manage, as this is a good way for their secret zero day to get uncovered. This has been typical of attacks in the past and will probably continue to do so. While somewhat rare these days, this could still create an impact if successful. The danger of pivoting is also considered a part of this.

## Stage VII - Risk and Impact Analysis
<!--
 * Qualify and quantify the business impact.
 * Countermeasure identification and residual impact.
 * Identify risk mitigation strategies. Effectiveness of mitigation(s) vs cost to implement mitigation.
 * Residual benefits, as the implementation of a change to single component as a mitigation effort could mean increased security for other systems that access that component.

This covers the development of the rationale for mitigation based upon residual risk.
  -->
*Create a list of business impacts from the various scenarios outlined in the previous stages.*

Script kiddie attacks would be prevalent and certainly the most common, with their impact if successful as being more damage to reputation than financial or anything else.  Pivoting is also a danger that could involve law enforcement.

Nation state and blackhat attacks are possible as they occur now, with the main impact being from attempts to pivot impacting other systems, either at GitLab or other NMRC systems. It is possible that a pivot attack against a 3rd party could also be involved, leading to potentially involve law enforcement.

*Countermeasures for each risk need to be determined.*

The first countermeasure should be to harden the system against initial compromise. Since the main point of attack would be via network services, tight controls on these services need to be in place. This would include hardened settings for web-based services and disabling of services that are not needed or require access from the public. 

The second countermeasure is controling network access during the installation process itself. Network access is needed during installation, but this can be adjusted on the fly as the GitLab application is installed (for example network access is needed to ensure letsencrypt is installed properly) and firewall rules can also be tweaked on the fly as needed. The problem is that this has not been done before and may not be practical, so this comes across as just an idea more than a plan.

The third countermeasure is to limit access to authorized personnel only. This can be controlled by imposing some limits on sign-ups to the new system, although from a network perspective this is not considered possible. The "front door" to the GitLab application is the same web interface for authorized users as anonymous consumers of publicly-accessible data. After installation a firewall rule can be put in place to restricted access during the process of limiting new user sign-up, and then after this and a few other hardening steps, opened back up.

The fourth countermeasure is already in place via a server that is configured to monitor all outbound traffic from the public servers, noting traffic levels, types of traffic, and frequency. While flawed, this is not a perfect solution as it is prone to false positives especially as new applications, services, and systems are put online.

The fifth countermeasure is to ensure automated and unattended updates run daily to help ensure the latest patches are applied. This should also be fairly easy to set up as it is set up on numerous servers and workstations already.

The sixth countermeasure is a good backup solution. As it stands now, this is done manually, and while thorough the entire process is labor intensive and inefficient. An automated backup solution is needed.

*From the mitigations, rank them as to how easily or hard it could be to mitigate the risk, and bear in mind that an easy-to-implement mitigation that solves a highly unlikely risk scenario should could still be implemented, improving security overall.*

The rankings from easiest to hardest are as follows:
 - Fourth countermeasure.
 - Second countermeasure.
 - Third countermeasure.
 - First countermeasure.
 - Fifth countermeasure.
 - Sixth countermeasure.

*Document mitigations that will be applied to address the risks, based upon likelihood, cost to business, etc, but consider residual benefits of some mitigations if a hard and costly mitigation will improve the security of additional systems/subsystems/processes etc.*


These are more or less included in the countermeasures above. The main countermeasure is "hardening" which will be detailed in a separate document as it is completed, as to fully harden may include such items as research and even code changes.

<!--
The following three sections are entirely optional, but might be useful in helping out the overall process. Uncomment and fill out as you see fit.
  -->

<!--
### Blind threat model.

GitLab’s best practices applied to components of the project.

 * Maps key goals of app or service and correlates to clear technical standards for architecture, hardening of server/service, app framework, containers, etc.
 * Best practices per component. For example, TLS settings that are set to a GitLab standard, noting if our own standard is higher or lower than industry best practices.
 * Best practices for coding are applied here as well, the “Sec” part of DevSecOps and our integration of this into CI/CD.
 * SAST/DAST policies and scopes. We can "eat our own dogfood" to improve the quality of the changes we implement.

Apply to Stage I & Stage II above.
  -->
<!--
### Evidence Driven threat model.

Proof of a threat via numerous indicators as opposed to just theory or conjecture.

 * Integrate threat log data analysis.
 * Focus on logs that support attack vector with the greatest motives (e.g. TLS MITM vs Injection-based attacks).
 * Correlate threat intel for foreseeing trends of attacks for target applications that are related to project components.

Apply to Stage III, IV, V above.
  -->
<!-->
### Full Risk Based Threat Model

 * Statistical/probabilistic analysis on threat data & attack effectiveness
 * Consider non-traditional threat vectors
 * This includes threat intel, H1 trends, existing logs
 * Substantiate the threats that are defined with real data

Apply to all stages above.
  -->
<!-- 
During the initial uses of this template, I'd like to monitor how they are working, so unless there is a privacy reason not to include me, please leave the cc in for my own benefit for now. This will eventually be removed later.

Major changes to the framework please submit the changes in the handbook!
  -->
